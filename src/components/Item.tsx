import React from 'react';
import { IonList, IonItem, IonLabel, IonContent } from '@ionic/react';

type datas = {   
    label: string;
    music: string;
};
const data: datas[] = [{ label:'Pokémon Yellow',music:'music1' },
    { label: 'Mega Man X', music: 'music2' },
    { label: 'The Legend of Zelda', music:'music3'}
    ];

export const Item: React.FC = (props) => {
    return (
         <IonContent>
        <IonList>
            {data.map((ele, i) => (
                <IonItem key={i}>
                    <IonLabel>{ele.label}</IonLabel>
                </IonItem>
            ))}
        </IonList>
    </IonContent>
    )
}

export const Music: React.FC = (props) => {
    return (
        <IonContent>
            <IonList>
                {data.map((ele, i) => (
                    <IonItem key={i}>
                        <IonLabel>{ele.music}</IonLabel>
                    </IonItem>
                ))}
            </IonList>
        </IonContent>
    )
}

