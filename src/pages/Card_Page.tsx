import React from 'react';
import Card from '../components/Card'
import { IonApp, IonContent } from '@ionic/react';

const Cards = [
    {
        subtitle: "Subtitle1",
        title: "Title1",
        content: "Content1"
    },
    {
        subtitle: "Subtitle2",
        title: "Title2",
        content: "Content2"
    }
]
const Card_Page: React.FC = () => {
    return (
        <IonApp>
            <IonContent>
                <Card subtitle={Cards[0]} />
                <Card subtitle={Cards[1]} />
            </IonContent>
        </IonApp>
    );
};
export default Card_Page;