import {
   IonContent, IonButton, IonApp
} from '@ionic/react';
import React, { useState } from 'react'
import {Item,Music} from '../components/Item'

const List: React.FC = () => {
    const [show,setShow] = useState(false)
    const [show1,setShow1] = useState(false)
    return (
        <IonApp>
            <IonContent className="ion-padding">
                <IonButton shape="round"  onClick={() => setShow(!show)}>Names</IonButton>
                <IonButton shape="round"  color="danger" onClick={() => setShow1(!show1)}>Music</IonButton>
                {show && <Item />}
                {show1 && <Music />}
            </IonContent>
        </IonApp>
    );
};

export default List;

