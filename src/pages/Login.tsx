import {
    IonPage, IonHeader, IonToolbar, IonTitle, IonContent, IonInput, IonButton,
    IonRow, IonCol, IonIcon, IonItem,
} from '@ionic/react';
import { personCircle } from "ionicons/icons";
import './Login.css';
import React, { useEffect, useState } from 'react'
import { Link} from 'react-router-dom'
import  {login}  from '../data/data.json'

    interface InitialData {
        email:string,
        password:string
    }

    let jsonData: InitialData;

    const Login: React.FC = () => {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    // const jsonData:Array<Object> = [];
  
    const loginUser = () => {
        // let history = useHistory;
        console.log(Object.values(jsonData))

        if(email===jsonData.email && password===jsonData.password){
            // console.log(jsonData)
           alert("logged in successfully")
        //    history.push('/Categories')
           window.location.href='/Categories'
        }
        else{
           alert("Please enter correct email or password")
        }
    }
 
   const getData =() => {
     try{
        return login;
     }
     catch(e){
     return e
     }
   }
    useEffect(() => {
        //  jsonData = getData();
        const data = getData();
        console.log(data[0]);
        jsonData=data[0];
        // console.log(data[0].email)
        // console.log(jsonData[0].email) 
    },[])

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle color="primary" className="title">
                        Login
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonRow>
                <IonCol>
                    <IonIcon color="primary"
                        style={{ fontSize: "100px", margin: "0 150px" }}
                        icon={personCircle}
                    />
                </IonCol>
            </IonRow>
            <IonContent className="ion-padding">
                <IonItem>
                    <IonInput type="email" placeholder="Enter email" onIonChange={(e: any) => setEmail(e.target.value)}></IonInput>
                </IonItem>
                <IonItem>
                    <IonInput type="password" placeholder="Enter Password" onIonChange={(e: any) => setPassword(e.target.value)}></IonInput>
                </IonItem>
                <IonButton className="btn" shape="round" expand="full" class="ion-margin-top"
                    onClick={loginUser}>Login</IonButton>
                <p>New here? <Link to="/Register">Register</Link></p>
            </IonContent>
        </IonPage>
    );
};

export default Login;