import {
    IonPage, IonHeader, IonToolbar, IonTitle, IonContent, IonInput, IonButton,
    IonRow, IonCol, IonIcon, IonItem,
} from '@ionic/react';
import { personCircle } from "ionicons/icons";
import './Login.css';
import React, { useState } from 'react'
import {Link} from 'react-router-dom'

const Register: React.FC = () => {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [cpassword, setCPassword] = useState<string>('');


    const loginUser = () => {
        console.log(email, password,cpassword)
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle color="primary" className="title">
                      Register
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonRow>
                <IonCol>
                    <IonIcon color="primary"
                        style={{ fontSize: "100px", margin: "0 150px" }}
                        icon={personCircle}
                    />
                </IonCol>
            </IonRow>
            <IonContent className="ion-padding">
                <IonItem>
                    <IonInput type="email" placeholder="Enter email" onIonChange={(e: any) => setEmail(e.target.value)}></IonInput>
                </IonItem>
                <IonItem>
                    <IonInput type="password" placeholder="Enter Password" onIonChange={(e: any) => setPassword(e.target.value)}></IonInput>
                </IonItem>
                <IonItem>
                    <IonInput type="password" placeholder="Enter Confirm Password" onIonChange={(e: any) => setCPassword(e.target.value)}></IonInput>
                </IonItem>
                <IonButton className="btn" shape="round" expand="full" class="ion-margin-top"
                    onClick={loginUser}>Register</IonButton>
                    <p>Already have an account? <Link to="/Login">Login</Link></p>
            </IonContent>
        </IonPage>
    );
};

export default Register;