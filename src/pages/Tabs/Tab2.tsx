import React from 'react';
import Card from '../../components/Card'
import { IonContent, IonHeader, IonApp, IonTitle, IonToolbar} from '@ionic/react';


 const Tab2: React.FC = () => {
     const Cards = [
        {
           subtitle:"Subtitle1",
           title:"Title1",
           content:"Content1"
        },
        {
            subtitle: "Subtitle2",
            title: "Title2",
            content: "Content2"
        }
    ]
    return (
        <IonApp>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>CardExamples</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                    <Card subtitle={Cards[0]} />
                    <Card subtitle={Cards[1]} />               
            </IonContent>
        </IonApp>
    );
};

export default Tab2;